<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <script src = "http://v16he2v4.esy.es/web/NMCDashboard/jquery.js" type="text/javascript"></script>

    <title>Vikrela</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Paper Dashboard core CSS    -->
    <link href="assets/css/paper-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />


    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/themify-icons.css" rel="stylesheet">

</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-background-color="white" data-active-color="danger">

    <!--
        Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
        Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
    -->

        <div class="sidebar-wrapper">
            <div class="logo">
                <a href="#" class="simple-text">
                    Vikrela
                </a>
            </div>

            <ul class="nav">
                <li>
                    <a href="dashboard.html">
                        <i class="ti-panel"></i>
                        <p>Scan</p>
                    </a>
                </li>
                <li>
                    <a href="user.html">
                        <i class="ti-user"></i>
                        <p>Hawker enrollment</p>
                    </a>
                </li>
                <li>
                <li class="active">
                    <a href="table.html">
                        <i class="ti-view-list-alt"></i>
                        <p>Rent</p>
                    </a>
                </li>
                <li>
                    <a href="typography.html">
                        <i class="ti-text"></i>
                        <p>Inspection</p>
                    </a>
                </li>
                <li>
                    <a href="icons.html">
                        <i class="ti-pencil-alt2"></i>
                        <p>Proximity scan</p>
                    </a>
                </li>
                <li>
                    <a href="maps.html">
                        <i class="ti-map"></i>
                        <p>Complaint</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand" href="#">Dashboard</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="ti-panel"></i>
                                <p>NMC</p>
                            </a>
                        </li>
                        <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="ti-bell"></i>
                                    <p>Vikrela</p>
                                    <b class="caret"></b>
                              </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="ti-settings"></i>
                                <p>Clients</p>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-3 col-sm-6" onclick="choice('rentCollected')">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="icon-big icon-warning text-center">
                                            <img src ="thumb_up1600.png" style = "width: 20px; height: 30px" />
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="numbers">
                                            <p >Fare</p>
                                            <p id = "legalcount">0</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr />
                                    <div class="stats">
                                        <i class="ti-reload"></i> Updated now
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   <div class="col-lg-3 col-sm-6" onclick="choice('rentCollected')">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="icon-big icon-warning text-center">
                                            <img src ="stop.png" style = "width: 20px; height: 30px" />
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="numbers">
                                            <p >Violation 1</p>
                                            <p id = "legalcount">0</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr />
                                    <div class="stats">
                                        <i class="ti-reload"></i> Updated now
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   <div class="col-lg-3 col-sm-6" onclick="choice('rentCollected')">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="icon-big icon-warning text-center">
                                            <img src ="Exclamation_mark_2.svg.png" style = "width: 20px; height: 30px" />
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="numbers">
                                            <p >Violation 2</p>
                                            <p id = "legalcount">0</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr />
                                    <div class="stats">
                                        <i class="ti-reload"></i> Updated now
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6" onclick="choice('noRentCollected')">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="icon-big icon-success text-center">
                                            <img src = "bulldozer.jpg" style = "width: 20px; height: 30px"/>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="numbers">
                                            <p>Enforce</p>
                                            <p id = "illegalcount">0</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr />
                                    <div class="stats">
                                        <i class="ti-calendar"></i>Updated now
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
       
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="content table-responsive table-full-width" id = "showData" style="height: 60vh;overflow: auto">
                        
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


                </div>
                <div class="row">
                </div>
            </div>
        </div>


        
    </div>
</div>

</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

    <!--  Checkbox, Radio & Switch Plugins -->
    <script src="assets/js/bootstrap-checkbox-radio.js"></script>

    <!--  Charts Plugin -->
    <script src="assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

    <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
    <script src="assets/js/paper-dashboard.js"></script>

    <!-- Paper Dashboard DEMO methods, don't include it in your project! -->
    <script src="assets/js/demo.js"></script>

    <script type="text/javascript">
        var numberOfRows = 0;
        var typeSelected='rentCollected';
        $(document).ready(function(){
            waitForMsg(numberOfRows);

        });



        function waitForMsg (numberOfRows) {
        $.ajax({
            type: "GET",
            url: "getAllInspectedHawkers.php?row_no="+numberOfRows+"&type="+typeSelected,
            async: true,
            cache : false,
            dataType : 'JSON',
            success : function (data){
                var incomingData = data;
                numberOfRows = Object.keys(incomingData).length;
                
                document.getElementById("legalcount").innerHTML = incomingData[0]['rentCollected'];
                document.getElementById("illegalcount").innerHTML = incomingData[0]['rentNotCollected'];
                setTimeout("waitForMsg(numberOfRows)" ,3000);
                //alert(incomingData);
                CreateTableFromJSON(incomingData);
            },
            erorr : function  (XMLHttpRequest,textStatus, erorrThrown) {
                alert("erorr :" + textStatus + "(" + erorrThrown + ")");
                setTimeout("waitForMsg(numberOfRows)", 15000);
       
            }

        });




        }

        function choice(choice){
            typeSelected = choice;
            waitForMsg(0);
        }


            function CreateTableFromJSON(JSONARRAY) {
                  
                  var myBooks = JSONARRAY;
                  
                  
                  // EXTRACT VALUE FOR HTML HEADER. 
                  // ('Book ID', 'Book Name', 'Category' and 'Price')
                  var table = document.createElement("table")
                  var col = [];
                        for (var i = 0; i < myBooks.length; i++) {
                              for (var key in myBooks[i]) {
                                    if (col.indexOf(key) === -1) {
                                    col.push(key);
                              }
                        }
                  }
                  // CREATE DYNAMIC TABLE.

                  // CREATE HTML TABLE HEADER ROW USING THE EXTRACTED HEADERS ABOVE.
                  var tr = table.insertRow(-1);                   // TABLE ROW.

                  for (var i = 0; i < col.length; i++) {
                        var th = document.createElement("th");      // TABLE HEADER.
                        th.innerHTML = "<center>"+col[i]+"</center>";
                        th.style.width = "100px";
                        th.style.padding = "20px";
                        tr.style.background = "#4CAF50";
                        tr.appendChild(th);
                  }

                  // ADD JSON DATA TO THE TABLE AS ROWS.
                  for (var i = 0; i < myBooks.length; i++) {
                        tr = table.insertRow(-1);
                        for (var j = 0; j < col.length; j++) {
                              var tabCell = tr.insertCell(-1);

                              tabCell.innerHTML = "<center>"+myBooks[i][col[j]]+"</center>";
                              if(i%2==0){
                                tabCell.style.background = "#f2f2f2";
                              }
                              tabCell.style.padding= "15px"
                        }
                  }

                  // FINALLY ADD THE NEWLY CREATED TABLE WITH JSON DATA TO A CONTAINER.
                  var divContainer = document.getElementById("showData");
                  divContainer.innerHTML = "";
                  divContainer.appendChild(table);
            }



    </script>

</html>
